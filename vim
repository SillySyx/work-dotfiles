Alt + i		toggle floating terminal

^ = Control
_ = Space

[Normal mode]
_x		Close buffer (tab)
_fm		Format buffer
_ch		Cheat sheet
_gt		git status :D
_gb		git blame line
_cm		git commits (history)
_x		close buffer (tab)
_h		cmd line (horizontal)
_v		cmd line (vertical)
_e		focus file tree
_/		toggle comment (works in visual mode as well)

_ff		find file
_fb		find file (from open buffers)
_fw		find in any file
_fz		find in current buffer

_ca		LSP code action
_gr		LSP show references
_ds		LSP show diagnostics list
^wd		LSP show diagnostics

^n		toggle file tree
^h		move to left window
^l		move to right window

:123	go to line 123
/...	search forward (repeat with just /)
?...	search backward (repeat with just ?)
n       next match
b       prev match
f.      find next char
F.      find prev char

i		insert before cursor
I		insert before beginning of line
a		insert after cursor
A		insert after end of line
o		insert new line below cursor
O		insert new line above cursor

V		select entire line  
vi(		select everything within ( ... )
vi{		select everything within { ... }
vi"		select everything within "..."

gU		make uppercase
gg		go to start of file
G		go to end of file
gd		go to definition
_cc		jump to current context (outside block?)

p		put (paste)
y		yank (copy)
yy		yank line
d		delete (cut)
dd		delete line
u		undo
^r		redo
x		remove char

w		next word
b		previous word
S		go to correct indentation position
s		go to correct indentation position

H		place cursor high on screen
M		place cursor middle on screen
L		place cursor low on screen

^u		move cursor up half a screen
^d		move cursor down half a screen

dw		delete next word
db		delete previous word
di"		delete inside quote
di(		delete inside ( ... )
di{		delete inside { ... }
yi"		copy/yank inside quote
dgg		delete to the start of file (saves to clipboard)
dG		delete to end of file (saves it to clipboard)

^wc		close window
^wq		quit window (useful in terminal)
^wo		close all other windows

J		move line below up to current line


[Insert mode]
^s		save buffer
^o		run normal mode commands

^w		delete previous word
^u		delete everything infront of cursor

^x -> ^o	LSP omnifunc (completion)


[Visual mode] (Ctrl + v)
>		indent selected lines
<		unindent selected lines


[Terminal mode]
^/ -> ^N	Enter normal mode


[Other]

How to search and delete value multiple times
/hello                      # search for hello
dgn                         # delete next match
.                           # repeat previous command


How to replace all occurances in file
:%s/word-to-find/replace-with/g


How to surround current selection
c -> "" -> p/P              # P for pasting behind cursor, p for pasting infront of cursor
c -> () -> p/P              # Anything can surround the selection


