---@type ChadrcConfig
local M = {}

M.base46 = {
      theme = "everblush",
      transparency = true,
      nvdash = {
          load_on_startup = true,
      },
}

return M