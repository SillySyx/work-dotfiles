require "nvchad.mappings"

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")

map({ "n", "i", "v" }, "<F5>", "<cmd>lua require'dap'.continue()<CR>")
map({ "n", "i", "v" }, "<F9>", "<cmd>lua require'dap'.toggle_breakpoint()<CR>")
map({ "n", "i", "v" }, "<F10>", "<cmd>lua require'dap'.step_over()<CR>")
map({ "n", "i", "v" }, "<F11>", "<cmd>lua require'dap'.step_into()<CR>")
map({ "n", "i", "v" }, "<F12>", "<cmd>lua require'dap'.step_out()<CR>")

map({ "n", "i", "v" }, "<S-Up>", "<Nop>")
map({ "n", "i", "v" }, "<S-Down>", "<Nop>")
map({ "n", "i", "v" }, "<S-Left>", "<Nop>")
map({ "n", "i", "v" }, "<S-Right>", "<Nop>")

map("v", "<M-Down>", ":m '>+1<CR>gv=gv")
map("v", "<M-Up>", ":m '<-2<CR>gv=gv")
map("v", "<M-Left>", "<gv")
map("v", "<M-Right>", ">gv")